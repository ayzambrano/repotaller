var productosObtenidos = [];
/*function getProductos(){
  var url= "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request= new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyStage ==4 && this.status ==200){
    //  console.log(request.responseText);
    //  productosObtenidos = request.responseText;
    //  procesarProductos();
    }
  }
  request.open("GET",url,true);
  request.send();
  alert("");
  //alert(request.responseText);
  //procesarProductos();
}
*/
function getProductos(){
    var request = new XMLHttpRequest();
    var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Products";

    request.open("GET",url,true); //async
    //callback
    request.onreadystatechange = function(){
    if(request.readyState === 4){
       try {
         var response = JSON.parse(request.responseText);
         if(request.status === 200){
             productosObtenidos = response;
             procesarProductos();
         }
       } catch(exception){
           alert("Product list not available");
       }
     }
   };
   request.send();
};


function procesarProductos(){
  var divTabla =  document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var i=0; i < productosObtenidos.value.length;i++){
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = productosObtenidos.value[i].ProductName;
    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = productosObtenidos.value[i].UnitPrice;
    var columnaStock = document.createElement("td");
    columnaStock.innerText = productosObtenidos.value[i].UnitInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
