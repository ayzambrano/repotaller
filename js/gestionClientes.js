var clientesObtenidos = [];
/*function getCustomer(){
  var url= "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var request= new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyStage ==4 && this.status ==200){
      console.log(request.responseText);
    }
  }
  request.open("GET",url,true);
  request.send();
};*/

function getCustomer(){
    var request = new XMLHttpRequest();
    //var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
    var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";

    request.open("GET",url,true); //async
    //callback
    request.onreadystatechange = function(){
    if(request.readyState === 4){
       try {
         var response = JSON.parse(request.responseText);
         if(request.status === 200){
             clientesObtenidos = response;
             procesarClientes();
         }
       } catch(exception){
           alert("Users list not available");
       }
     }
   };
   request.send();
};

function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  var divTabla =  document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var i=0; i < clientesObtenidos.value.length;i++){
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = clientesObtenidos.value[i].ContactName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = clientesObtenidos.value[i].City;
    var columnaBandera = document.createElement("td");
    var imgBandera =  document.createElement("img");
    imgBandera.classList.add("flag");
    if(clientesObtenidos.value[i].Country=="UK"){
      imgBandera.src=rutaBandera+"United-Kingdom.png";
    }else{
      imgBandera.src=rutaBandera+clientesObtenidos.value[i].Country+".png";
    }
    columnaBandera.appendChild(imgBandera);
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
